var canvas = document.getElementById("source");
scale_btn = document.getElementById("scale_btn");
btn_in = document.getElementById("zoom_in");
btn_out = document.getElementById("zoom_out");
zoom_tool = document.getElementById("zoom_tool");
percent_scale = document.getElementById("percent_zoom");

var startscale = false;

var img = new Image();

const X = img.width;
const Y = img.height;

canvas.height = window.innerHeight - 100;
canvas.width = window.innerWidth;

window.onload = function() {
  var ctx = canvas.getContext("2d");
  trackTransforms(ctx);

  function redraw() {
    // clear canvas
    var p1 = ctx.transformedPoint(0, 0);
    var p2 = ctx.transformedPoint(canvas.width, canvas.height);
    ctx.clearRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);

    ctx.save();
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.restore();

    ctx.drawImage(img, 0, 0);
  }
  redraw();

  var lastX = canvas.width / 2,
    lastY = canvas.height / 2;

  var dragStart, dragged;

  scale_btn.addEventListener("click", function(e) {
    if (startscale == false) {
      scale_btn.innerText = "Zooming";
      scale_btn.classList.remove("btn-outline-primary");
      scale_btn.classList.add("btn-primary");
      startscale = true;
      zoom_tool.classList.remove("invisible");
      zoom_tool.classList.add("visible");
    } else {
      scale_btn.innerText = "Zoom";
      scale_btn.classList.remove("btn-primary");
      scale_btn.classList.add("btn-outline-primary");
      startscale = false;
      zoom_tool.classList.remove("visible");
      zoom_tool.classList.add("invisible");
    }
  });

  percent_scale.addEventListener("keyup", function(e) {
    if (e.keyCode === 13) {
      e.preventDefault();
      var temp = percent_scale.value / 100;
      ctx.setTransform(1, 0, 0, 1, 0, 0);
      ctx.scale(temp, temp);

      redraw();
    }
  });

  btn_in.addEventListener(
    "mousedown",
    function(e) {
      if (startscale == true) {
        lastX = e.offsetX || e.pageX - canvas.offsetLeft;
        lastY = e.offsetY || e.pageY - canvas.offsetTop;
      }
    },
    false
  );

  btn_out.addEventListener(
    "mousedown",
    function(e) {
      if (startscale == true) {
        lastX = e.offsetX || e.pageX - canvas.offsetLeft;
        lastY = e.offsetY || e.pageY - canvas.offsetTop;
      }
    },
    false
  );

  canvas.addEventListener(
    "mousedown",
    function(e) {
      if (startscale == true) {
        document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect =
          "none";
        lastX = e.offsetX || e.pageX - canvas.offsetLeft;
        lastY = e.offsetY || e.pageY - canvas.offsetTop;
        dragStart = ctx.transformedPoint(lastX, lastY);
        dragged = false;
      }
    },
    false
  );

  canvas.addEventListener(
    "mousemove",
    function(e) {
      if (startscale == true) {
        lastX = e.offsetX || e.pageX - canvas.offsetLeft;
        lastY = e.offsetY || e.pageY - canvas.offsetTop;
        dragged = true;
        if (dragStart) {
          var pt = ctx.transformedPoint(lastX, lastY);
          ctx.translate(pt.x - dragStart.x, pt.y - dragStart.y);
          redraw();
        }
      }
    },
    false
  );

  canvas.addEventListener(
    "mouseup",
    function(e) {
      if ((startscale = true)) {
        dragStart = null;
        // if (!dragged) zoom(e.shiftKey ? -1 : 1);
      }
    },
    false
  );

  btn_in.addEventListener(
    "mouseup",
    function(e) {
      if ((startscale = true)) {
        zoom(e.shiftKey ? -1 : 1);
      }
    },
    false
  );

  btn_out.addEventListener(
    "mouseup",
    function(e) {
      if ((startscale = true)) {
        zoom(e.shiftKey ? 1 : -1);
      }
    },
    false
  );

  var scaleFactor = 1.1;

  var zoom = function(clicks) {
    var pt = ctx.transformedPoint(lastX, lastY);
    ctx.translate(pt.x, pt.y);
    var factor = Math.pow(scaleFactor, clicks);
    ctx.scale(factor, factor);
    ctx.translate(-pt.x, -pt.y);
    redraw();
  };

  var handleScroll = function(e) {
    if (startscale == true) {
      var delta = e.wheelDelta ? e.wheelDelta / 40 : e.detail ? -e.detail : 0;
      if (delta) zoom(delta);
      return e.preventDefault() && false;
    }
  };

  canvas.addEventListener("DOMMouseScroll", handleScroll, false);
  canvas.addEventListener("mousewheel", handleScroll, false);
};

img.src =
  "https://www.acornnaturalists.com/pub/media/catalog/product/cache/afad95d7734d2fa6d0a8ba78597182b7/t/-/t-15171_2.jpg";

function trackTransforms(ctx) {
  var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  var xform = svg.createSVGMatrix();
  ctx.getTransform = function() {
    return xform;
  };

  var savedTransforms = [];
  var save = ctx.save;
  ctx.save = function() {
    savedTransforms.push(xform.translate(0, 0));
    return save.call(ctx);
  };

  var restore = ctx.restore;
  ctx.restore = function() {
    xform = savedTransforms.pop();
    return restore.call(ctx);
  };

  var scale = ctx.scale;
  ctx.scale = function(sx, sy) {
    xform = xform.scaleNonUniform(sx, sy);
    return scale.call(ctx, sx, sy);
  };

  var rotate = ctx.rotate;
  ctx.rotate = function(radians) {
    xform = xform.rotate((radians * 180) / Math.PI);
    return rotate.call(ctx, radians);
  };

  var translate = ctx.translate;
  ctx.translate = function(dx, dy) {
    xform = xform.translate(dx, dy);
    return translate.call(ctx, dx, dy);
  };

  var transform = ctx.transform;
  ctx.transform = function(a, b, c, d, e, f) {
    var m2 = svg.createSVGMatrix();
    m2.a = a;
    m2.b = b;
    m2.c = c;
    m2.d = d;
    m2.e = e;
    m2.f = f;
    xform = xform.multiply(m2);
    return transform.call(ctx, a, b, c, d, e, f);
  };

  var setTransform = ctx.setTransform;
  ctx.setTransform = function(a, b, c, d, e, f) {
    xform.a = a;
    xform.b = b;
    xform.c = c;
    xform.d = d;
    xform.e = e;
    xform.f = f;
    return setTransform.call(ctx, a, b, c, d, e, f);
  };

  var pt = svg.createSVGPoint();
  ctx.transformedPoint = function(x, y) {
    pt.x = x;
    pt.y = y;
    return pt.matrixTransform(xform.inverse());
  };
}
